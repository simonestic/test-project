<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <title>Icon kit preview | gulp-svg-sprites</title>
    <link href="css/sprite.css" rel="stylesheet" type="text/css" media="all"/>
    <style type="text/css">@CHARSET "UTF-8";
    * {
        box-sizing: border-box;
    }
    body {
        padding: 0;
        margin: 0;
        color: #666;
        background: #fafafa;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 1em;
        line-height: 1.4
    }

    header {
        display: block;
        padding: 3em 3em 2em 3em;
        background-color: #fff
    }

    header p {
        margin: 0 0 0 0;
    }

    nav {
        font-size: .7em;
        display: block;
        width: 100%;
        margin: 0 0 2em 0
    }

    nav a {
        display: inline-block;
        text-decoration: none;
        margin-left: 2em;
        color: #0f7595;
        white-space: nowrap
    }

    nav a:hover {
        text-decoration: underline
    }

    nav a.current {
        font-weight: bold;
        text-decoration: underline;
        color: #666
    }

    section {
        border-top: 1px solid #eee;
        padding: 2em 3em 0 3em
    }

    ul {
        margin: 0;
        padding: 0
    }

    .icon-boxes li {
        display: inline-block;
        background-color: #fff;
        position: relative;
        margin: 0 2em 2em 0;
        vertical-align: top;
        border: 1px solid #ccc;
        padding: 1em 1em 1em 1em;
        cursor: default
    }

    .icon-box {
        margin: 0;
        width: 144px;
        height: 144px;
        position: relative;
        background: #ccc url(data:image/gif;base64,R0lGODlhDAAMAIAAAMzMzP///yH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjEgNjQuMTQwOTQ5LCAyMDEwLzEyLzA3LTEwOjU3OjAxICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1LjEgV2luZG93cyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDozQjk4OTI0MUY5NTIxMUUyQkJDMEI5NEFEM0Y1QTYwQyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDozQjk4OTI0MkY5NTIxMUUyQkJDMEI5NEFEM0Y1QTYwQyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjNCOTg5MjNGRjk1MjExRTJCQkMwQjk0QUQzRjVBNjBDIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjNCOTg5MjQwRjk1MjExRTJCQkMwQjk0QUQzRjVBNjBDIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEAAAAAAAsAAAAAAwADAAAAhaEH6mHmmzcgzJAUG/NVGrfOZ8YLlABADs=) top left repeat;
        border: 1px solid #ccc;
        display: table-cell;
        vertical-align: middle;
        text-align: center
    }
    .icon-box.inverted {
        background: black;
    }

    .icon {
        display: inline;
        display: inline-block
    }

    .snippet-popover {
        position: absolute;
        display: none;
        opacity: 0;
        bottom: 0;
        left: 0;
        background: white;
        border: 1px solid #ccc;
        padding: 16px;
        z-index: 1;
        transition: all .3s;
    }
    .snippet-popover.active {
        display: block;
        opacity: 1;
    }

    pre {
        margin: 0;
        margin-bottom: 20px;
    }

    h2 {
        margin: 0;
        padding: 5px 0;
        font-size: 1em;
        font-weight: normal;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    h4 {
        margin: 0;
    }

    footer {
        display: block;
        margin: 0;
        padding: 0 3em 3em 3em
    }

    footer p {
        margin: 0;
        font-size: .7em
    }

    footer a {
        color: #0f7595;
        margin-left: 0
    }
    </style>

</head>
<body>
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="0" height="0" style="position:absolute">
	
		<symbol id="svg_aparat" viewBox="0 0 27.1 27.1">
			<path d="M11.7.8L9.1.1C6.9-.4 4.7.8 4.2 3l-.7 2.6c2-2.6 5-4.3 8.2-4.8M26.2 11.7l.7-2.6c.6-2.1-.7-4.3-2.8-4.9l-2.6-.7c2.5 2 4.2 4.9 4.7 8.2M.8 15.4L.1 18c-.6 2.1.7 4.3 2.8 4.9l2.6.7c-2.5-2-4.2-5-4.7-8.2M15.4 26.2l2.6.7c2.1.6 4.3-.7 4.9-2.8l.7-2.6c-2 2.5-5 4.2-8.2 4.7"></path><path d="M16.6 2.1C10.3.4 3.9 4.1 2.2 10.4c-1.7 6.3 2 12.7 8.3 14.4 6.3 1.7 12.7-2 14.4-8.3 1.7-6.2-2-12.7-8.3-14.4 0 .1 0 .1 0 0m-5.2 3.2c1.7.5 2.8 2.2 2.3 4-.5 1.7-2.2 2.8-4 2.3-1.7-.5-2.7-2.3-2.2-4 .4-1.8 2.2-2.8 3.9-2.3M7.6 19.6c-1.7-.5-2.8-2.2-2.3-4 .5-1.7 2.2-2.8 4-2.3 1.7.5 2.8 2.2 2.3 4-.5 1.8-2.3 2.8-4 2.3m4.5-6.4c.2-.8 1-1.2 1.8-1 .8.2 1.2 1 1 1.8-.2.8-1 1.2-1.8 1-.4-.1-.7-.3-.9-.7-.1-.4-.2-.8-.1-1.1m3.6 8.6c-1.7-.5-2.8-2.2-2.3-4 .5-1.7 2.2-2.8 4-2.3 1.7.5 2.8 2.2 2.3 4-.6 1.7-2.3 2.7-4 2.3m2.1-8.1c-1.7-.5-2.8-2.2-2.3-4 .5-1.7 2.2-2.7 4-2.3 1.7.5 2.8 2.2 2.3 4-.5 1.7-2.3 2.8-4 2.3"></path>
		</symbol>
	
		<symbol id="svg_instagram" viewBox="0 0 20.5 20.5">
			<path d="M14.4 0H6.1C2.699 0 0 2.801 0 6.1v8.3c0 3.4 2.801 6.1 6.1 6.1h8.3c3.4 0 6.1-2.801 6.1-6.1V6.1A6.06 6.06 0 0 0 14.4 0zm4.1 14.5c0 2.301-1.801 4.1-4.1 4.1H6.1c-2.301 0-4.1-1.801-4.1-4.1V6.199C2 4 3.801 2.1 6.1 2.1h8.3c2.199 0 4.1 1.801 4.1 4.1v8.3z"></path><path d="M10.201 4.898c-2.9 0-5.301 2.4-5.301 5.301S7.3 15.5 10.201 15.5s5.301-2.4 5.301-5.301c.098-2.9-2.301-5.199-5.301-5.301zm0 8.602C8.4 13.5 6.9 12 6.9 10.199s1.5-3.301 3.301-3.301 3.301 1.5 3.301 3.301C13.5 12.1 12 13.5 10.201 13.5z"></path><circle cx="15.6" cy="5" r="1.301"></circle>
		</symbol>
	
		<symbol id="svg_search" viewBox="0 0 19 19">
			<path d="M7.53 14.561c-1.932 0-3.604-.693-4.971-2.06C1.193 11.134.5 9.462.5 7.53s.693-3.604 2.059-4.971C3.926 1.193 5.599.5 7.53.5s3.604.693 4.971 2.059c1.366 1.367 2.06 3.039 2.06 4.971s-.693 3.604-2.06 4.971-3.039 2.06-4.971 2.06zm0-12.061c-1.402 0-2.565.482-3.557 1.473C2.982 4.965 2.5 6.128 2.5 7.53c0 1.401.482 2.565 1.473 3.557.992.991 2.155 1.474 3.557 1.474 1.401 0 2.564-.482 3.557-1.474.991-.991 1.474-2.155 1.474-3.557 0-1.402-.482-2.565-1.474-3.557C10.095 2.982 8.932 2.5 7.53 2.5zM17.5 18.5a.997.997 0 0 1-.707-.293l-4.021-4.02a.999.999 0 1 1 1.414-1.414l4.021 4.02A.999.999 0 0 1 17.5 18.5z"></path>
		</symbol>
	
		<symbol id="svg_telegram" viewBox="0 0 20.7 18">
			<path d="M.4 8.6l4.8 1.8L7 16.3c.1.3.4.5.7.4.1 0 .1-.1.2-.1l2.7-2.2c.3-.2.7-.2 1 0l4.8 3.5c.3.2.6.1.8-.1 0-.1.1-.1.1-.2L20.8.7c.1-.3-.1-.6-.4-.7h-.3L.4 7.6c-.3.1-.5.4-.4.7.1.2.2.3.4.3zm6.3.9L16 3.7c.1-.1.2 0 .2 0v.2L8.5 11c-.3.3-.4.6-.5 1l-.3 2c0 .1-.1.2-.3.2-.1 0-.2-.1-.2-.2l-1-3.5c-.1-.4.1-.8.5-1z"></path>
		</symbol>
	
		<symbol id="svg_twitter" viewBox="0 0 20.2 16.8">
			<path d="M19.9 2c-.4.2-.9.4-1.4.5.5-.5.9-1.1 1.1-1.8 0-.1 0-.2-.1-.3h-.2c-.7.4-1.4.7-2.1.9h-.1c-.1 0-.3-.1-.4-.1C15.8.4 14.8 0 13.8 0c-.5 0-.9.1-1.4.2-1.4.4-2.5 1.6-2.8 3-.2.6-.2 1.1-.2 1.7V5h-.1C6.2 4.7 3.4 3.2 1.4.8c-.1-.1-.2-.1-.3 0L1 .9c-1 1.7-.7 3.9.7 5.3-.3-.1-.7-.2-1-.4-.1-.1-.2 0-.3.1V6c0 1.7 1 3.3 2.5 4-.2 0-.5 0-.7-.1-.2 0-.3.1-.3.2v.1c.5 1.6 1.8 2.7 3.4 3-1.3.9-2.9 1.4-4.5 1.4H.3c-.2 0-.3.1-.3.3 0 .1 0 .3.2.4 1.8 1.1 3.9 1.6 6 1.6 1.8 0 3.5-.4 5.1-1.1 1.4-.7 2.7-1.6 3.8-2.8 1-1.1 1.8-2.4 2.3-3.8.5-1.3.8-2.8.8-4.2v-.2c0-.2.1-.4.3-.6.7-.5 1.3-1.2 1.7-1.9.1-.1 0-.2-.1-.3-.1-.1-.2-.1-.2 0z"></path>
		</symbol>
	
		<symbol id="svg_user" viewBox="0 0 18 21">
			<path d="M9 21c-4.152 0-9-1.109-9-4.235 0-4.858 4.038-8.812 9-8.812 4.963 0 9 3.953 9 8.812C18 19.891 13.151 21 9 21zM9 9.953c-3.86 0-7 3.056-7 6.812C2 17.844 4.813 19 9 19c4.188 0 7-1.156 7-2.235 0-3.756-3.141-6.812-7-6.812z"></path><path fill="#FFF" d="M13.6 5.468c0 1.234-.449 2.287-1.347 3.16C11.354 9.5 10.271 9.936 9 9.936S6.646 9.5 5.747 8.627C4.849 7.755 4.4 6.702 4.4 5.468c0-1.233.449-2.287 1.347-3.159C6.646 1.436 7.729 1 9 1s2.354.436 3.253 1.309c.897.872 1.347 1.925 1.347 3.159z"></path><path d="M9 10.936c-1.533 0-2.862-.535-3.949-1.591C3.955 8.281 3.4 6.976 3.4 5.468s.555-2.813 1.65-3.876C6.138.535 7.466 0 9 0c1.533 0 2.862.535 3.949 1.591 1.096 1.064 1.65 2.369 1.65 3.876 0 1.508-.555 2.813-1.649 3.876-1.089 1.058-2.417 1.593-3.95 1.593zM9 2c-1.009 0-1.845.335-2.556 1.026C5.741 3.708 5.4 4.507 5.4 5.468c0 .961.341 1.76 1.044 2.442.711.691 1.547 1.026 2.556 1.026s1.845-.335 2.557-1.026c.701-.682 1.043-1.481 1.043-2.442 0-.96-.342-1.759-1.044-2.442C10.846 2.335 10.009 2 9 2z"></path>
		</symbol>
	
</svg>

<header>
    <h1 id="logo">Gulp SVG Sprites <small>(6 icons in &lt;symbol&gt; mode)</small></h1>
    <h4>Files Generated:</h4>
    <ol>
        <li><a href="sprite.svg">sprite.svg</a></li>
    </ol>
    <h4>Usage:</h4>
    <ol>
        <li>
            Include the <strong>contents</strong> of the <a href="sprite.svg">SVG file</a> just after the opening <code>&lt;body&gt;</code> tag.
        </li>
        <li>
            Paste one of the snippets anywhere into your website.
        </li>
    </ol>

</header>
<section>
    <ul class="icon-boxes">
        

        <li title="svg_aparat">
            <div class="icon-box" id="icon-box-svg_aparat">
                <svg class="icon svg_aparat">
                    <use xlink:href="#svg_aparat" />
                </svg>
            </div>
            <h2>svg_aparat</h2>
            <button onclick="showPopover('snippet-svg_aparat')">Show Snippet</button>
            <br/>
            <button onclick="invertBackground('icon-box-svg_aparat')">Invert Background</button>
            <div class="snippet-popover" id="snippet-svg_aparat">
                <pre><code>&lt;svg class=&quot;icon svg_aparat&quot;&gt;&lt;use xlink:href=&quot;#svg_aparat&quot;&gt;&lt;/use&gt;&lt;/svg&gt;</code></pre>
                <button onclick="hidePopover()">Close</button>
            </div>
        </li>
        

        <li title="svg_instagram">
            <div class="icon-box" id="icon-box-svg_instagram">
                <svg class="icon svg_instagram">
                    <use xlink:href="#svg_instagram" />
                </svg>
            </div>
            <h2>svg_instagram</h2>
            <button onclick="showPopover('snippet-svg_instagram')">Show Snippet</button>
            <br/>
            <button onclick="invertBackground('icon-box-svg_instagram')">Invert Background</button>
            <div class="snippet-popover" id="snippet-svg_instagram">
                <pre><code>&lt;svg class=&quot;icon svg_instagram&quot;&gt;&lt;use xlink:href=&quot;#svg_instagram&quot;&gt;&lt;/use&gt;&lt;/svg&gt;</code></pre>
                <button onclick="hidePopover()">Close</button>
            </div>
        </li>
        

        <li title="svg_search">
            <div class="icon-box" id="icon-box-svg_search">
                <svg class="icon svg_search">
                    <use xlink:href="#svg_search" />
                </svg>
            </div>
            <h2>svg_search</h2>
            <button onclick="showPopover('snippet-svg_search')">Show Snippet</button>
            <br/>
            <button onclick="invertBackground('icon-box-svg_search')">Invert Background</button>
            <div class="snippet-popover" id="snippet-svg_search">
                <pre><code>&lt;svg class=&quot;icon svg_search&quot;&gt;&lt;use xlink:href=&quot;#svg_search&quot;&gt;&lt;/use&gt;&lt;/svg&gt;</code></pre>
                <button onclick="hidePopover()">Close</button>
            </div>
        </li>
        

        <li title="svg_telegram">
            <div class="icon-box" id="icon-box-svg_telegram">
                <svg class="icon svg_telegram">
                    <use xlink:href="#svg_telegram" />
                </svg>
            </div>
            <h2>svg_telegram</h2>
            <button onclick="showPopover('snippet-svg_telegram')">Show Snippet</button>
            <br/>
            <button onclick="invertBackground('icon-box-svg_telegram')">Invert Background</button>
            <div class="snippet-popover" id="snippet-svg_telegram">
                <pre><code>&lt;svg class=&quot;icon svg_telegram&quot;&gt;&lt;use xlink:href=&quot;#svg_telegram&quot;&gt;&lt;/use&gt;&lt;/svg&gt;</code></pre>
                <button onclick="hidePopover()">Close</button>
            </div>
        </li>
        

        <li title="svg_twitter">
            <div class="icon-box" id="icon-box-svg_twitter">
                <svg class="icon svg_twitter">
                    <use xlink:href="#svg_twitter" />
                </svg>
            </div>
            <h2>svg_twitter</h2>
            <button onclick="showPopover('snippet-svg_twitter')">Show Snippet</button>
            <br/>
            <button onclick="invertBackground('icon-box-svg_twitter')">Invert Background</button>
            <div class="snippet-popover" id="snippet-svg_twitter">
                <pre><code>&lt;svg class=&quot;icon svg_twitter&quot;&gt;&lt;use xlink:href=&quot;#svg_twitter&quot;&gt;&lt;/use&gt;&lt;/svg&gt;</code></pre>
                <button onclick="hidePopover()">Close</button>
            </div>
        </li>
        

        <li title="svg_user">
            <div class="icon-box" id="icon-box-svg_user">
                <svg class="icon svg_user">
                    <use xlink:href="#svg_user" />
                </svg>
            </div>
            <h2>svg_user</h2>
            <button onclick="showPopover('snippet-svg_user')">Show Snippet</button>
            <br/>
            <button onclick="invertBackground('icon-box-svg_user')">Invert Background</button>
            <div class="snippet-popover" id="snippet-svg_user">
                <pre><code>&lt;svg class=&quot;icon svg_user&quot;&gt;&lt;use xlink:href=&quot;#svg_user&quot;&gt;&lt;/use&gt;&lt;/svg&gt;</code></pre>
                <button onclick="hidePopover()">Close</button>
            </div>
        </li>
        
    </ul>
</section>
<footer>
    <p>Generated by <a href="https://github.com/shakyshane/gulp-svg-sprites">Gulp Svg Sprites</a> - Preview page courtesy of <a href="http://iconizr.com" target="_blank">iconizr</a>.</p>
</footer>
<script type="text/javascript">
    var openSnippet;
    function showPopover(id) {
        openSnippet = document.getElementById(id);
        openSnippet.classList.add("active");
    }
    function hidePopover() {

        openSnippet.classList.remove("active");
    }
    function invertBackground(id) {
        var elem = document.getElementById(id);
        elem.classList.toggle("inverted");
    }
</script>
</body>
</html>
