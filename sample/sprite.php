<!DOCTYPE html>
<html lang="fa">
<head>
  <link rel="stylesheet" href="../template/css/main.css">
</head>
<body>

  <div class="page page-home">
    <svg class="icon"><use xlink:href="../template/images/sprite.svg#svg_search"></use></svg>
    <svg class="icon"><use xlink:href="../template/images/sprite.svg#svg_aparat"></use></svg>
    <svg class="icon"><use xlink:href="../template/images/sprite.svg#svg_instagram"></use></svg>
    <svg class="icon"><use xlink:href="../template/images/sprite.svg#svg_telegram"></use></svg>
    <svg class="icon"><use xlink:href="../template/images/sprite.svg#svg_twitter"></use></svg>
    <svg class="icon"><use xlink:href="../template/images/sprite.svg#svg_user"></use></svg>
  </div>

  <script src="../template/js/script.js"></script>
</body>
</html>
